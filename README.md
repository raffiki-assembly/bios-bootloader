# bios-bootloader

A Simple assembly that creates your own custom boot-loader.
Testing is with Bochs x86 Emulator.

## Compiling the Assembly code with Gnu-AS

Compiling the code we require GAS(gnu-assembler)
It involves 2 stages

* gas compiler
* ld - linker

# Compiling

```sh
as myfirst.s -o first.o
```

# linking

```sh
ld first.o -o first -Ttext=0x7c00 --oformat=binary
```

## Test with bochs emulator

To test your bootloader. The bochsrc file is preconfigure
to mount the firt bootloader file as `floppy a` and then 
later uses it as the first boot option when run.

```sh
bochs
```
