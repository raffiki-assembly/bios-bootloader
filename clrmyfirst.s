# This assembly uses bios to boot 
# and print a message. References 
# from Professor Ralf Brown’s Interrupt List
# You can say: This code creates a bootloader 
#--------------------------------------------- 
.code16
.global _start

_start:
	mov $0x0700, %ax  # from int-10h/AH=07h and AL=00h ; Tells bios to clear the screen
	mov $0x00, %bh   # character attribute = black on black
 	mov $0x0000, %cx # row = 0, col = 0
 	mov $0x1847, %dx   # row = 24 (0x18), col = 79 (0x4f)
 	int $0x10
 	# Screen cleared now we can pretty print on fresh display
	mov len, %cx  #from int-10h/AX=13h; cx requires number of chars to print
	mov $0x0000, %dx  # from int-10h/AX=13h; dx contains row and column to start on 
	mov $0x005e, %bx  # BH is page number and BL is colours from BIOS_color_attributes
	mov $msg, %bp  # from int-10h/AX=13h; bp stores the text to display
	mov $0x1301, %ax  # Sets the interrupt function to be called when int is called(13h = VIDEO - WRITE STRING)
	int $0x10
loop: jmp loop

msg: .ascii "My first simple assembly BootLoader...\n\r"
     .ascii "And it's awesome!!!"
len: .short .- msg # this checks how many hex length we have in msg 

.org 510  # padding the remaining blocks to the required boot-sector size < 512-bytes 
.word 0xAA55  # this line enables the boot flag 
