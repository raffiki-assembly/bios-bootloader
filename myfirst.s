# This assembly uses bios to boot 
# and print a message. References 
# from Professor Ralf Brown’s Interrupt List
# You can say: This code creates a bootloader 
#--------------------------------------------- 
.code16
.global _start

_start:
	mov len, %cx  #from int-10h/AX=13h; cx requires number of chars to print
	mov $0x1400, %dx  # from int-10h/AX=13h; dx contains row and column to start on 
	mov $0x005e, %bx  # BH is page number and BL is colours from BIOS_color_attributes
	mov $msg, %bp  # from int-10h/AX=13h; bp stores the text to display
	mov $0x1301, %ax
	int $0x10
loop: jmp loop

msg: .ascii "My first simple assembly BootLoader...\n\r"
     .ascii "And it's awesome!!!"
len: .short .- msg # this checks how many hex length we have in msg 

.org 510
.word 0xAA55
