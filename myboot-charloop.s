//.org 0x7c00
.code16

.section .text
.globl _start
_start:
	mov $message, %si
	call print_stuff

loop: jmp loop

print_stuff:
	xor %bx, %bx
.loops:
	lodsb
	cmp $0, %al
	je .done
	call print_char
	jmp .loops
.done:
	ret

print_char:
	mov  $0x0e, %ah
	int $0x10
	ret


message: .asciz "Hello World!\r\n"

_start_end: .set CODE_SIZE, _start_end - _start

.org (510 - $CODE_SIZE)
.word 0xAA55

